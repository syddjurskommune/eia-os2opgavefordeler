package dk.syddjurs.eia.os2opgavefordeler.converter;

import dk.syddjurs.eia.os2opgavefordeler.model.Employee;
import dk.syddjurs.eia.os2opgavefordeler.model.Organization;
import dk.syddjurs.eia.os2opgavefordeler.route.OS2OpgavefordelerMain;
import org.apache.camel.Converter;
import org.apache.camel.Exchange;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Converter
public class OrganizationConverter {

    @Converter
    public static Organization[] toOrganizationList(List<Map<String, Object>> organizationList) {
        List<Organization> result = new ArrayList<>();;
        if( organizationList != null )
        {
            for (Map<String, Object> organizationMap : organizationList) {
                result.add(createOrganizationFromMap(organizationMap));
            }
        }
        return result.toArray(new Organization[result.size()]);
    }

    @Converter
    public static Organization toOrganizationTree(Organization[] organizationList, Exchange exchange) {
        return buildOrganizationTree(organizationList, (Employee[]) exchange.getIn().getHeader(OS2OpgavefordelerMain.EMPLOYEE_DATA_HEADER));
    }


    public static Organization buildOrganizationTree(Organization[] organizationList, Employee[] employeeList)
    {
        Organization result = null;
        if (organizationList != null && employeeList != null ) {
            for (Organization organization : organizationList) {
                addEmployess(organization,employeeList);
                if (organization.getParentOrganization() != null) {
                    Organization parentOrganization = findOrganization(organizationList, organization.getParentOrganization().getBusinessKey());
                    parentOrganization.addChild(organization);
                } else {
                    // organization has no parent, so it must be the root organization
                    result = organization;
                }
            }
        }
        return result;
    }

    private static void addEmployess(Organization organization, Employee[] employeeList) {
        for (Employee employee : employeeList) {
            // set the manager
            if (organization.getManager() != null && organization.getManager().getBusinessKey().equals(employee.getBusinessKey()))
            {
                organization.setManager(employee);
            }
            // add employee
            if (employee.getOrganization().getBusinessKey().equals(organization.getBusinessKey())) {
                organization.addEmployee(employee);
            }
        }
    }

    private static Organization findOrganization(Organization[] organizationList, String businessKey) {
        Organization result = null;
        for (Organization organization : organizationList) {
            if( organization.getBusinessKey().equals(businessKey)){
                return organization;
            }
        }
        return result;
    }

    private static Organization createOrganizationFromMap(Map<String, Object> organizationMap)
    {        
        Organization organization = new Organization();
        if( organizationMap.get("businessKey") != null ) organization.setBusinessKey(String.valueOf(organizationMap.get("businessKey")));
        if( organizationMap.get("parentBusinessKey") != null )
        {
            var parentOrganization = new Organization();
            parentOrganization.setBusinessKey(String.valueOf(organizationMap.get("parentBusinessKey")));
            organization.setParentOrganization(parentOrganization);
        }
        if( organizationMap.get("name") != null ) organization.setName(String.valueOf(organizationMap.get("name")));
        if( organizationMap.get("email") != null ) organization.setEmail(String.valueOf(organizationMap.get("email")));
        if( organizationMap.get("phone") != null ) organization.setPhone(String.valueOf(organizationMap.get("phone")));
        if( organizationMap.get("esdhId") != null ) organization.setEsdhId(String.valueOf(organizationMap.get("esdhId")));
        if( organizationMap.get("esdhLabel") != null ) organization.setEsdhLabel(String.valueOf(organizationMap.get("esdhLabel")));
        if( organizationMap.get("managerBusinessKey") != null )
        {
            var manager = new Employee();
            manager.setBusinessKey(String.valueOf(organizationMap.get("managerBusinessKey")));
            organization.setManager(manager);   
        }
        return organization;
    }


}
