package dk.syddjurs.eia.os2opgavefordeler.converter;

import dk.syddjurs.eia.os2opgavefordeler.model.Employee;
import dk.syddjurs.eia.os2opgavefordeler.model.Organization;
import org.apache.camel.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Converter
public class EmployeeConverter {

    @Converter
    public static Employee[] toEmployeeList(List<Map<String, Object>> employeeList) {
        List<Employee> result = new ArrayList<>();;
        if( employeeList != null )
        {
            for (Map<String, Object> employeeMap : employeeList) {
                result.add(createEmployeeFromMap(employeeMap));
            }
        }
        return result.toArray(new Employee[result.size()]);
    }

    private static Employee createEmployeeFromMap( Map<String, Object> employeeMap )
    {
        var employee = new Employee();
        if( employeeMap.get("businessKey") != null ) employee.setBusinessKey(String.valueOf(employeeMap.get("businessKey")));
        if( employeeMap.get("name") != null ) employee.setName(String.valueOf(employeeMap.get("name")));
        if( employeeMap.get("email") != null ) employee.setEmail(String.valueOf(employeeMap.get("email")));
        if( employeeMap.get("initials") != null ) employee.setInitials(String.valueOf(employeeMap.get("initials")));
        if( employeeMap.get("jobTitle") != null ) employee.setJobTitle(String.valueOf(employeeMap.get("jobTitle")));
        if( employeeMap.get("phone") != null ) employee.setPhone(String.valueOf(employeeMap.get("phone")));
        if( employeeMap.get("esdhId") != null ) employee.setEsdhId(String.valueOf(employeeMap.get("esdhId")));
        if( employeeMap.get("organizationBusinessKey") != null )
        {
            var organization = new Organization();
            organization.setBusinessKey(String.valueOf(employeeMap.get("organizationBusinessKey")));
            employee.setOrganization(organization);
        }
        return employee;
    }


}
