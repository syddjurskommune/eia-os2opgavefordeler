package dk.syddjurs.eia.os2opgavefordeler.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Employee {
    private String businessKey;
    private String name;
    private String email;
    private String initials;
    private String jobTitle;
    private String phone;
    private String esdhId;

    @JsonIgnore
    private Organization organization;
}