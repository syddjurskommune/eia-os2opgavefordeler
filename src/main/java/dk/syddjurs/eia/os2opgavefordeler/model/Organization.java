package dk.syddjurs.eia.os2opgavefordeler.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Organization {
    private String businessKey;
    private String name;
    private String email;
    private String phone;
    private String esdhId;
    private String esdhLabel;
    private Employee manager;
    private List<Employee> employees = new ArrayList<>();
    private List<Organization> children = new ArrayList<>();

    @JsonIgnore
    private Organization parentOrganization;

    public void addChild(Organization childOrganization) {
        children.add(childOrganization);
        childOrganization.setParentOrganization(this);
    }

    public void addEmployee(Employee employee) {
        employees.add(employee);
        employee.setOrganization(this);
    }
}
