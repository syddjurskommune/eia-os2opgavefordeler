package dk.syddjurs.eia.os2opgavefordeler.config;

import com.microsoft.sqlserver.jdbc.SQLServerDataSource;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class SOFDConfiguration
{
    @ConfigurationProperties(prefix = "sofd.datasource")
    @Bean
    public DataSource getSOFDDataSource() {
        return DataSourceBuilder.create().type(SQLServerDataSource.class).build();
    }

}