package dk.syddjurs.eia.os2opgavefordeler.route;

import dk.syddjurs.eia.os2opgavefordeler.UnexpectedEmployeeCountException;
import dk.syddjurs.eia.os2opgavefordeler.UnexpectedOrganizationCountException;
import dk.syddjurs.eia.os2opgavefordeler.model.Employee;
import dk.syddjurs.eia.os2opgavefordeler.model.Organization;
import org.apache.camel.Exchange;
import org.apache.camel.PropertyInject;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class OS2OpgavefordelerMain extends SpringRouteBuilder {

    public static final String ENDPOINT = "direct:eia.os2opgavefordeler.Main";
    public static final String ROUTE_ID = "EIA-OS2Opgavefordeler:Main";
    public static final String EMPLOYEE_DATA_HEADER = "EmployeeData";

    @Value("${organization-minium-count:0}")    
    private int organizationMinimumCount;
    @Value("${employee-minimum-count:0}")    
    private int employeeMinimumCount;

    @Override
    public void configure() throws Exception {
        /* @formatter:off */        
        JacksonDataFormat jacksonDataFormat = new JacksonDataFormat(Organization.class);
        jacksonDataFormat.setPrettyPrint(false);

        from(ENDPOINT)
        .routeId(ROUTE_ID)
        // get employee data from database
        .setBody(simple("SELECT * FROM os2opgavefordeler.Employee"))
        .to("jdbc:getSOFDDataSource")
        .choice()
            // Fail-safe mechanism that ensures that api is not invoked if employee count is lower than expected
            .when( simple("${body.size} < " + employeeMinimumCount ))
                .log("EmployeeCount: ${body.size}. MinimumEmployeeCount: " + employeeMinimumCount)
                .throwException( new UnexpectedEmployeeCountException() )
            .otherwise()
                .convertBodyTo(Employee[].class)
                // put employee data in header for later use by OrganizationConverter
                .setHeader(EMPLOYEE_DATA_HEADER, simple("${body}"))
                // get organisation data from database
                .setBody(simple("SELECT * FROM os2opgavefordeler.Organization"))
                .to("jdbc:getSOFDDataSource")
                .choice()
                    // Fail-safe mechanism that ensures that api is not invoked if organization count is lower than expected
                    .when( simple("${body.size} < " + organizationMinimumCount ))
                        .log("OrganizationCount: ${body.size}. MinimumOrgnizationCount: " + organizationMinimumCount)
                        .throwException( new UnexpectedOrganizationCountException() )
                    .otherwise()
                        .convertBodyTo(Organization[].class)
                        .convertBodyTo(Organization.class)
                        // remove headers to prevent them from being included in the http request
                        .removeHeader(EMPLOYEE_DATA_HEADER)
                        // convert body to json
                        .marshal(jacksonDataFormat)
                        .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
                        // invoke API
                        .to("https:{{api-url}}?httpClient.authenticationPreemptive=true&authMethod=Basic&authUsername={{api-username}}&authPassword={{api-password}}")
                        .log("Organization uploaded")
                .end()
        .end();
        /* @formatter:on */        
    }

}