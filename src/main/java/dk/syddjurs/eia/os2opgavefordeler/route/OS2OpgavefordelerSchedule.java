package dk.syddjurs.eia.os2opgavefordeler.route;

import org.apache.camel.spring.SpringRouteBuilder;
import org.springframework.stereotype.Component;

@Component
public class OS2OpgavefordelerSchedule extends SpringRouteBuilder {


    @Override
    public void configure() throws Exception {
        // one-time sync on startup
        from("timer:startupSync?repeatCount=1")
        .routeId("EIA-OS2Opgavefordeler:Startup-sync")
        .to(OS2OpgavefordelerMain.ENDPOINT);
        
        // scheduled sync
        from("quartz2:OS2OpgavefordelerSync?cron={{quartz-cron}}")
        .routeId("EIA-OS2Opgavefordeler:Schedule-sync")
        .to(OS2OpgavefordelerMain.ENDPOINT);
    }
}